public class WaitNotify {
    public WaitNotify() throws InterruptedException {
        Object sync = new Object();
        Thread ta = new ThreadA(sync);
        ta.start();
        Thread.sleep(2000);
        synchronized (sync) {
            Thread tb = new ThreadB(sync);
            tb.start();
            sync.notify();
        }
        //почему здесь монитор отдается A, а не B?
        Thread.sleep(2000);
        synchronized (sync) {
            sync.notify();
        }
    }

    public static void main(String[] args) throws InterruptedException {
        new WaitNotify();
    }

    class ThreadA extends Thread {
        Object sync;

        public ThreadA(Object sync) {
            this.sync = sync;
        }

        @Override
        public void run() {
            super.run();
            synchronized (sync) {
                System.out.println("A begin");
                try {
                    sync.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println("A end");
            }
        }
    }

    class ThreadB extends Thread {
        Object sync;

        public ThreadB(Object sync) {
            this.sync = sync;
        }

        @Override
        public void run() {
            super.run();
            synchronized (sync) {
                System.out.println("B begin");
                try {
                    sync.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println("B end");
            }
        }
    }
}
